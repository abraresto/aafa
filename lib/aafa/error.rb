module Aafa
  class Error < StandardError
    attr_reader :response, :code, :description

    def initialize(code)
      @code = code
    end
  end
end