module ActionDispatch::Routing
  class Mapper
    # Includes mount_aafa_oauth method for routes. This method is responsible to
    # generate all needed routes for oauth
    def mount_aafa_oauth(options = {})
      skip_routes = options[:except].is_a?(Array) ? options[:except] : [options[:except]]
      controllers = options[:controllers] || {}

      oauth_new_controller = controllers[:oauth_new] || 'aafa/oauth/auth'
      get  'oauth/new'          => "#{oauth_new_controller}#new",  :as => 'oauth_new'
      post 'oauth/authorize'    => 'aafa/oauth/auth#create',       :as => 'oauth_authorize'
      post 'oauth/token'        => 'aafa/oauth/token#create',      :as => 'oauth_token'

      unless skip_routes.include?(:client_apps)
        oauth_client_apps = controllers[:oauth_client_apps] ||'aafa/oauth/admin'
        resources :api_admin, :controller => oauth_client_apps, :path => '/api/admin'
      end
      unless skip_routes.include?(:docs)
        oauth_docs = controllers[:oauth_docs] ||'aafa/oauth/docs'
        resources :api_docs,        :controller => oauth_docs, :only => [:index, :show], :path => '/api/docs'
      end
      unless skip_routes.include?(:tests)
        oauth_tests = controllers[:oauth_tests] ||'aafa/oauth/tests'
        resources :oauth_tests,       :controller => oauth_tests, :only => [:index, :show, :create, :destroy]
      end
    end
  end
end
