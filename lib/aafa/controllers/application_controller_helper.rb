# this concern gets put into ApplicationController

module Aafa
  module Controllers
    module ApplicationControllerHelper
      extend ActiveSupport::Concern

      include Aafa::Controllers::Concerns::Permissions
      include Aafa::Controllers::Concerns::ErrorMessages
      include Aafa::Controllers::Concerns::RateLimits

      included do
        around_filter      :oauth_auth!
        skip_before_filter :verify_authenticity_token, :if => :valid_oauth?
      end

      def aafa_authenticate_user!
        Aafa.authenticate_user_method.call(self)
        true
      end

      module ClassMethods
        def allow_oauth!(options = {})
          prepend_before_filter :allow_oauth, options
        end
        
        def allow_oauth_with_skip_exception!(options = {})
          prepend_before_filter :allow_oauth_with_skip_exception, options
        end

        def disallow_oauth!(options = {})
          prepend_before_filter :disallow_oauth,  options
          skip_before_filter    :allow_oauth,     options
        end

        def authenticate_oauth_client!(options = {})
          prepend_before_filter :authenticate_oauth_client,  options
        end
      end

      protected

      def authenticate_oauth_client
        @application = Aafa::Oauth::ClientApp.authenticate(params[:client_id], params[:client_secret])

        unless @application.present?
          render :json => {:error => {:message => 'Invalid client credentials', :code => 106, :description => 'The client credentials used in the request is incorrect'}}, :status => :unauthorized
          false
        end
      end
      
      def current_client_app
        @application || oauth_client_app || Aafa::Oauth::ClientApp.authenticate(params[:client_id], params[:client_secret])
      end

      def oauth_fail_request!
        render :json => {:error => {:message => 'Bad authentication data', :code => 103, :description => generate_oauth_error_message!}}, :status => :unauthorized
        false
      end

      def allow_oauth?
        @use_oauth ||= false
      end
      
      def with_exception?
        @with_exception.nil? or @with_exception == false ? false : true
      end


      def valid_oauth?
        oauth? && oauth_user.present? && oauth_client_not_expired? && oauth_client_has_permissions? && oauth_client_under_rate_limit?
      end

      def oauth_client_not_expired?
        oauth_access_grant.not_expired?
      end

      def disallow_oauth
        @use_oauth = false
      end

      def allow_oauth
        @use_oauth = true
      end
      
      def allow_oauth_with_skip_exception
        @use_oauth = true
        @with_exception = false
      end

      def oauth_access_token
        params[:access_token] || oauth_access_token_from_header
      end

      # grabs access_token from header if one is present
      def oauth_access_token_from_header
        auth_header = request.env["HTTP_AUTHORIZATION"]||""
        match       = auth_header.match(/token\W*([^\W]*)/) || auth_header.match(/^Bearer\s(.*)/) || auth_header.match(Aafa.header_auth_regex)
        return match[1] if match.present?
        false
      end

      def oauth?
        allow_oauth? && oauth_access_token.present?
      end

      # Override with custom logic to exclude or allow applications from exchanging
      # passwords for access_tokens
      def oauth_valid_password_auth?(client_id, client_secret)
        true
      end

      def oauth_access_grant
        @oauth_access_grant ||= Aafa::Oauth::AuthGrant.find_for_token(oauth_access_token)
      end

      def oauth_client_app
        return false      if oauth_access_grant.blank?
        @oauth_client_app ||= oauth_access_grant.client_application
      end

      def oauth_user
        return false if oauth_access_grant.blank?
        @oauth_user  ||= oauth_access_grant.user
      end

      def oauth_auth!
        if defined?(user_signed_in?) && user_signed_in?
          yield
        elsif valid_oauth?
          ::Aafa.login(self, oauth_user)
          yield
          ::Aafa.logout(self, oauth_user)
        elsif with_exception? and allow_oauth? and not valid_oauth?
          render :json => {:error => {:message => 'Invalid or expired token', :code => 104, :description => 'The access token used in the request is incorrect or has expired.'}}, :status => :unauthorized
          false
        else
          yield
        end
      end

    end
  end
end