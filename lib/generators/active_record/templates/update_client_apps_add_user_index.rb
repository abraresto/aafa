class UpdateClientAppsAddUserIndex < ActiveRecord::Migration
  def change
    add_index :aafa_client_apps, :user_id
  end
end
