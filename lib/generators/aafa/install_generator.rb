require 'securerandom'

module Aafa
  module Generators
    class InstallGenerator < Rails::Generators::Base
      source_root File.expand_path("../../templates", __FILE__)

      desc "Creates an aafa initializer"
      class_option :orm

      def copy_initializer
        template "aafa.rb", "config/initializers/aafa.rb"
      end

      def run_other_generators
        generate "active_record:aafa"
      end

      def add_aafa_routes
        aafa_routes = "mount_aafa_oauth"
        route aafa_routes
      end
    end
  end
end

