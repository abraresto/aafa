# This controller is where clients can exchange
# codes and refresh_tokens for access_tokens

class Aafa::Oauth::TokenController < AafaController
  before_filter      :aafa_authenticate_user!,    :except => [:create]
  skip_before_filter :verify_authenticity_token,  :only   => [:create]
  authenticate_oauth_client!

  def create
    # Find the client application
    auth_grant  = auth_grant_for(@application, params)

    if auth_grant.present?
      auth_grant.refresh!
      render :json => { access_token: auth_grant.access_token,
        # http://tools.ietf.org/html/rfc6749#section-5.1
        token_type:    Aafa.token_type || 'bearer',
        refresh_token: auth_grant.refresh_token,
        expires_in:    auth_grant.expires_in }
    else
      render_error debug_msg(params)
    end
  end

  private

  def auth_grant_for(application, params)
    if params[:code]
      Aafa::Oauth::AuthGrant.find_by_code_app(params[:code], application)
    elsif params[:refresh_token]
      Aafa::Oauth::AuthGrant.find_by_refresh_app(params[:refresh_token], application)
    elsif params[:password].present? || params[:grant_type] == "password"|| params[:grant_type] == "bearer"
      return false unless Aafa.password_exchange_enabled?
      return false unless oauth_valid_password_auth?(params[:client_id], params[:client_secret])
      user       = ::Aafa.find_user_for_all_auths!(self, params)
      return false unless user.present?
      auth_grant = Aafa::Oauth::AuthGrant.find_or_create_by_user_app(user, application)
      auth_grant.update_permissions if auth_grant.present?
      auth_grant
    end
  end

  def debug_msg(options)
    msg = "Could not find a user that belongs to this application"
    msg << " & has a refresh_token=#{options[:refresh_token]}" if options[:refresh_token]
    msg << " & has been granted a code=#{options[:code]}"      if options[:code]
    msg << " using username and password"                      if options[:password]
    msg << "."
    msg
  end

  def render_error(msg)
    render :json => {:error => {:message => 'Bad authentication data', :code => 103, :description => msg}}, :status => :unauthorized
  end

end
