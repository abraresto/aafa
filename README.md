## AAFA (AbraAuth For API)

API version 1 for mobile apps - Feb 2014

## Why would I use this?
Lets say we've built a Rails app, awesome. Now you want to build a mobile app on say, the iPhone... cool.

## Install

Gemfile

```ruby
gem 'aafa'
```

Then run

```shell
$ bundle install
```

and don't forget

```shell
$ rails g aafa:install
```

This will put a file in `initializers/aafa.rb`, generate some migrations, and add `mount_aafa_oauth` to your routes.


Now we're ready to migrate the database

```shell
$ rake db:migrate
```

This will add `Aafa::Oauth::AuthGrant` and `Aafa::Oauth::ClientApp` to your database. An iPhone app would need to register for a `client_id` and `client_secret` before using OAuth as a ClientApp. Once created they could get authorization from users by going through the OAuth flow, thus creating AuthGrants. In other words, a ClientApp has many users through AuthGrants.


## Use it
AAFA comes with built in documentation, so if you start your server you can view the docs at [http://localhost:3000/api/docs](http://localhost:3000/api/docs)..
yeah, you need to login to see it - see Rachmat for password details

## Customizing Documentation / add your API resource documentation to the docs
By default AAFA ships with views and controllers much like Devise. You can use the built in AAFA views.
[current AAFA views](https://bitbucket.org/abra_rm/aafa) find it under app/views.

To do change you will need to copy the view to your Views directory.

# Admin panel
[http://localhost:3000/api/admin](http://localhost:3000/api/admin)
- see Rachmat for password details

Don’t continue until you’ve registered a client app. visit the admin panel page. Enter in the name of your application. For this example, we will use foo; you can change this later if you desire.
Hit enter and you should see a screen that has your application name along with a client id and a secret.

In this version of API AUTH, only app provider owner which can access the API..

Future dream.. User traffic monitor on admin panel.. yess, you can have awesome feature like this.



## Setup

Go to `initializers/aafa.rb` and configure your app for your authentication scheme. If you're not using devise, see "Custom Auth" below.

```ruby
Aafa.setup do |config|
  config.auth_strategy = :devise
end
```


Now in your controllers you can allow OAuth access using the same syntax of the rails `before_filter`

```ruby
class UsersController < ApplicationController
  allow_oauth!  :only => [:show]
end
```


You can also disallow OAuth on specific actions. Disallowing will always over-ride allowing.


```ruby
class ProductsController < ApplicationController
  disallow_oauth!   :only => [:create]
end
```

By default, all OAuth access is blacklisted. To whitelist all access, add `allow_oauth!` to your `ApplicationController` (this is not recommended). The best practice is to add `allow_oauth!` or `disallow_oauth` to each and every controller.

That should be all you need to do to get set up. Congrats, you're now able to authenticate users using OAuth!!



## Permissions

When a user authenticates with a client, they are automatically granting read permission to any action that you `allow_oauth!`. Read-only clients are restricted to using GET requests. By default, oPRO will ask users for write permission on a client by the client application. Client apps with `:write` permission can use all HTTP verbs including POST, PATCH, PUT, DESTROY on any url you whitelist using `allow_oauth!`.


### Custom Permissions

To remove write permissions, comment out this line in the oPRO initializer:

```ruby
config.request_permissions = [:write]
```

You can add custom permissions by adding to the array:

```ruby
config.request_permissions = [:write, :email, :picture, :whatever]
```

You can then restrict access using the custom permissions by calling `require_oauth_permissions`, which takes the same arguments as `before_filter`:

```ruby
require_oauth_permissions :email, :only => :index
```

You can also skip permissions using `skip_oauth_permissions`. By default, permissions will just check to see if a client has the permission and will allow the action if it is present. If you want to implement custom permission checks, you can write custom methods using the pattern `oauth_client_can_#{permission}?`. For example, if you were restricting the `:email` permission, you would create a method:

```ruby
def oauth_client_can_email?
  # ...
end
```

The result is expected to be true or false.


## Refresh Tokens

For added security, you can require access_tokens to be refreshed by client applications. This will help to mitigate the risk of a leaked access_token and enable an all around more secure system. This security comes at a price, however, since implementing the `refresh_token` functionality in a client can be more difficult.

By default, refresh tokens are enabled. You can disable them in your application and set the timeout period of the tokens by adding this line to your configuration:

```ruby
config.require_refresh_within = false
```

# Toggling Refresh Tokens

If you disable refresh tokens and then re-enable it you may have authorization grants that do not have a timeout listed, you can keep it like this or you can fix by iterating through all auth grants and setting their `access_token_expires_at` like this:

```ruby
Aafa::Oauth::AuthGrant.find_each(:conditions => "access_token_expires_at is null") do |grant|
  grant.access_token_expires_at = Time.now + ::Aafa.require_refresh_within
  grant.save
end
```

You may also need to inform clients that they need to update their credentials and start using refresh tokens.

## Password Token Exchange

If a client application has a user's password and username/email, they can exchange these for a token. This is much safer than storing the username and password on a local device, but it does not offer the traditional OAuth "Flow". Because of this, all available permissions will be granted to the client application. If you want to disable this feature you can set the configuration below to false:

```ruby
config.password_exchange_enabled = true
```

If you have this feature enabled, you can further control what applications can use the feature. Some providers may wish to have "Blessed" client applications that have this ability while restricting all other clients. To accomplish this, you can create a method in your ApplicationController called `oauth_valid_password_auth?` that accepts a client_id and client_secret and returns true or false based on whether that application can use password auth:

```ruby
def oauth_valid_password_auth?(client_id, client_secret)
  BLESSED_APP_IDS.include?(client_id)
end
```


 If you are using this password functionality without a supported authorization engine (like devise), you will need to add an additional method that supports validating whether or not a user's credentials are valid. The method for this is called `find_user_for_auth` and accepts a controller and the parameters. The output is expected to be a user. Add this to your config like you did to the other required methods in the "Custom Auth" section:

```ruby
config.find_user_for_auth do |controller, params|
  # user = User.find(params[:something])
  # return user.valid_password?(params[:password]) ? user : false
end
```

If you're authenticating by exchanging something other than a password (such as a facebook auth token), clients can still enable this functionality by setting `params[:grant_type] == 'password'` in their initial request. You can then use the `find_user_for_auth` method from above and implement your custom behavior. You can call `find_user_for_auth` multiple times and the application will try calling each auth method in order. It is suggested that you return from this block early if the params are missing a vital key like this:


```ruby
config.find_user_for_auth do |controller, params|
  return false if params[:fb_token].blank?
  User.where(:fb_token => params[:fb_token]).first
end
```


## Rate Limiting

If your API becomes a runaway success and people start abusing your API, you might choose to limit the rate that client applications can access your API. It is common for popular read-only APIs to have an hourly or daily rate limit to help prevent abuse. If you want this type of functionality, you can use oPRO's built in hooks: one to record the number of times a client application has accessed your API and another to let the application know if the Client app has gone over its allotted rate.

To record the number of times an application has accessed your site add this method to your ApplicationController:

```ruby
def oauth_client_record_access!(client_id, params)
  # implement your rate counting mechanism here
end
```

Then to let our server know if a given client has reached its limit, add the method below. The output is expected to be true if the client has gone over their limit and false if they have not:

```ruby
def oauth_client_rate_limited?(client_id, params)
  # implement your own custom rate limiting logic here
end
```

Rate limited clients will receive an "unsuccessful" response to any query with a message letting them know they've been rate limited. Using redis with a rotating key generator based on (hour, day, etc.) is one very common way to count accesses and implement the rate limits. Since there are so many different ways to implement this, we decided to give you a blank slate and let you implement it however you would like. The default is that apps are not rate limited, and in general unlimited API access is the way to go, but if you do find abusive behavior you can always easily add in a rate limit.



## Assumptions

* You have a user model and that is what you're authenticating
* You're using Active Record

## About

If you have a question file an issue find Alex,  I'm sitting beside him.