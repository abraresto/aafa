require 'test_helper'

class DummyPasswordExchangeTest < ActionDispatch::IntegrationTest
  test 'invalid client credentials' do
    user         = create_user
    auth_grant   = create_auth_grant_for_user(user)
    client_app   = auth_grant.application
    post oauth_token_path(client_id: client_app.client_id, client_secret: client_app.client_secret + 'foo', email: @created_user_email, password: @created_user_password)
    assert_equal 106, JSON.parse(response.body)["error"]["code"]
  end

  test 'valid client credentials and invalid user password' do
    user         = create_user
    auth_grant   = create_auth_grant_for_user(user)
    client_app   = auth_grant.application
    post oauth_token_path(client_id: client_app.client_id, client_secret: client_app.client_secret, email: @created_user_email, password: @created_user_password + 'foo')
    assert_equal 103, JSON.parse(response.body)["error"]["code"]
  end

  test 'valid client credentials and valid user password' do
    user         = create_user
    auth_grant   = create_auth_grant_for_user(user)
    client_app   = auth_grant.application
    post oauth_token_path(client_id: client_app.client_id, client_secret: client_app.client_secret, email: @created_user_email, password: @created_user_password)
    assert_equal 200, status
    assert_equal true, JSON.parse(response.body)["access_token"].present?
  end
end