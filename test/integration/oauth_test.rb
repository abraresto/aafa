require 'test_helper'

class DummyOauthTest < ActionDispatch::IntegrationTest

  test 'invalid auth_token should do nothing' do
    get root_path
    assert_equal 200, status
  end

  test 'valid auth token' do
    user         = create_user
    auth_grant   = create_auth_grant_for_user(user)
    access_token = auth_grant.access_token
    get root_path(access_token: access_token)
    assert_equal 200, status
  end

  test 'invalid auth token' do
    user         = create_user
    auth_grant   = create_auth_grant_for_user(user)
    access_token = auth_grant.access_token + "foo"
    get root_path(access_token: access_token)
    assert_equal 401, status
    assert_equal 104, JSON.parse(response.body)["error"]["code"]
  end
end
