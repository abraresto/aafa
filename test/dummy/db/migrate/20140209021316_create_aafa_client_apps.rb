class CreateAafaClientApps < ActiveRecord::Migration
  def change
    create_table :aafa_client_apps do |t|
      t.string  :name
      t.string  :app_id
      t.string  :app_secret
      t.text    :permissions
      t.timestamps
    end

    add_index :aafa_client_apps, :app_id, :unique => true
    add_index :aafa_client_apps, [:app_id, :app_secret], :unique => true
  end
end
