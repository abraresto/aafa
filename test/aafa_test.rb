require 'test_helper'

class AafaTest < ActiveSupport::TestCase
  test "truth" do
    assert_kind_of Module, Aafa
  end



end



class AafaSetupTest < ActiveSupport::TestCase

  test 'setting auth_strategy :devise' do
    Aafa.setup do |config|
      config.auth_strategy :devise
    end
    assert Aafa.login_method.present?
    assert Aafa.logout_method.present?
  end

end