# look in lib/aafa/rails/routes.rb
  # they get added to a users config/routes.rb when the user runs
  # rails g aafa:install
  # this functionality is added in `add_aafa_routes` of
  # lib/generators/aafa/install_generator.rb
Aafa::Engine.routes.draw do
  mount_aafa_oauth
end